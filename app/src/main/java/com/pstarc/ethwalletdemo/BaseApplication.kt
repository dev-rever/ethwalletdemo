package com.pstarc.ethwalletdemo

import android.app.Application

class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        LoginHelper.initialize(this)
        Web3Client.initialize()
    }
}