package com.pstarc.ethwalletdemo

import android.content.Context

object LoginHelper {

    private const val SP_NAME = "sp_name"
    private const val SP_ADDRESS = "sp_address"
    private const val SP_PRIVATE_KEY = "sp_private_key"

    private var _walletAddress: String? = null
    val walletAddress get() = _walletAddress

    private var _privateKey: String? = null
    val privateKey get() = _privateKey

    fun initialize(context: Context) {
        context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
            .apply {
                getString(SP_ADDRESS, null)
                    .takeIf { it.isNullOrEmpty().not() }
                    .run { _walletAddress = this }

                getString(SP_PRIVATE_KEY, null)
                    .takeIf { it.isNullOrEmpty().not() }
                    .run { _privateKey = this }
            }
    }

    fun login(context: Context, address: String, privateKey: String) {
        context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
            .edit()
            .putString(SP_ADDRESS, address)
            .putString(SP_PRIVATE_KEY, privateKey)
            .apply()
        _walletAddress = address
        _privateKey = privateKey
    }

    fun logout(context: Context) {
        context.getSharedPreferences(SP_NAME, Context.MODE_PRIVATE)
            .edit().clear().apply()
        _walletAddress = null
        _privateKey = null
    }
}