package com.pstarc.ethwalletdemo.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentResultListener
import androidx.lifecycle.lifecycleScope
import com.pstarc.ethwalletdemo.LoginHelper
import com.pstarc.ethwalletdemo.R
import com.pstarc.ethwalletdemo.databinding.ActivityHomeBinding
import com.pstarc.ethwalletdemo.repo.Web3Result
import com.pstarc.ethwalletdemo.showToast
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch

class HomeActivity : AppCompatActivity(), FragmentResultListener {

    private val binding: ActivityHomeBinding by lazy {
        ActivityHomeBinding.inflate(layoutInflater)
    }

    private val viewModel: HomeViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main)) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        supportFragmentManager.setFragmentResultListener(
            TransactionBottomSheet.REQUEST_KEY,
            this,
            this
        )

        setupView()
    }

    override fun onFragmentResult(requestKey: String, result: Bundle) {
        when (requestKey) {

            TransactionBottomSheet.REQUEST_KEY -> {

                val sheet = ProcessingBottomSheet.newInstance()
                sheet.show(supportFragmentManager, null)

                val tAddress = result.getString(TransactionBottomSheet.RESULT_ADDRESS) ?: return
                val quantity = result.getString(TransactionBottomSheet.RESULT_QUANTITY) ?: return

                lifecycleScope.launch(CoroutineExceptionHandler { _, throwable ->

                    throwable.printStackTrace()

                    sheet.dismiss()
                    showToast(throwable.message ?: "error")
                }) {
                    val abiResult = viewModel.sendEth(quantity, tAddress)
                    sheet.dismiss()
                    viewModel.refresh()

                    when (abiResult) {

                        is Web3Result.Success -> {
                            Toast.makeText(
                                this@HomeActivity,
                                "transaction success.",
                                Toast.LENGTH_SHORT
                            ).show()

                            TransactionSuccessBottomSheet.newInstance(
                                fromAddr = abiResult.data.from,
                                toAddr = abiResult.data.to,
                                txHash = abiResult.data.transactionHash,
                                block = abiResult.data.blockNumberRaw
                            )
                                .show(supportFragmentManager, null)
                        }

                        is Web3Result.Fail -> showToast(R.string.toast_transaction_fail)
                    }
                }
            }

            else -> {

            }
        }
    }

    private fun setupView() {
        with(binding) {
            viewModel.uiState
                .onEach {
                    if (it == null) {
                        shimmerBalance.startShimmer()
                        shimmerCurrencyName.startShimmer()
                        shimmerBalance.isVisible = true
                        shimmerCurrencyName.isVisible = true
                        txvBalance.isInvisible = true
                        txvCurrencyName.isInvisible = true
                        transaction.isGone = true
                        refresh.isGone = true
                        history.isGone = true
                    } else {
                        shimmerBalance.stopShimmer()
                        shimmerCurrencyName.stopShimmer()
                        shimmerBalance.isInvisible = true
                        shimmerCurrencyName.isInvisible = true
                        txvBalance.isVisible = true
                        txvCurrencyName.isVisible = true
                        txvBalance.text = it.balance
                        txvCurrencyName.text = it.currencyName
                        transaction.isVisible = true
                        refresh.isVisible = true
                        history.isVisible = true
                    }
                }
                .catch { it.printStackTrace() }
                .launchIn(lifecycleScope)

            transaction.setOnClickListener {
                TransactionBottomSheet.newInstance().show(supportFragmentManager, null)
            }

            refresh.setOnClickListener {
                viewModel.refresh()
            }

            history.setOnClickListener {
                showToast("coming soon...")
            }

            btnLogout.setOnClickListener {
                LoginHelper.logout(this@HomeActivity)
                MainActivity.start(this@HomeActivity)
                finish()
            }
        }
    }

    companion object {

        fun start(context: Context) {
            context.startActivity(Intent(context, HomeActivity::class.java))
        }
    }
}