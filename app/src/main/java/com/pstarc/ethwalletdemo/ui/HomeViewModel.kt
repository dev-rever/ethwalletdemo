package com.pstarc.ethwalletdemo.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pstarc.ethwalletdemo.repo.IWeb3Repository
import com.pstarc.ethwalletdemo.repo.Web3Repository
import com.pstarc.ethwalletdemo.repo.Web3Result
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import org.web3j.protocol.core.methods.response.TransactionReceipt
import org.web3j.utils.Convert
import java.math.BigDecimal
import java.math.BigInteger

class HomeViewModel : ViewModel() {

    data class UiState(
        val balance: String? = null,
        val currencyName: String? = null
    )

    private val web3Repo: IWeb3Repository = Web3Repository

    private val _uiState = MutableStateFlow<UiState?>(null)
    val uiState get() = _uiState.asStateFlow()

    init {
        refresh()
    }

    suspend fun sendEth(quantity: String, toAddress: String): Web3Result<TransactionReceipt> {
        return web3Repo.sendEth(quantity, toAddress)
    }

    fun refresh() {
        viewModelScope.launch {
            _uiState.update { null }
            delay(1000L) // fixme: Delayed viewing ui shimmer effect
            when (val result = web3Repo.fetchBalance()) {
                is Web3Result.Success -> {
                    _uiState.update {

                        it?.copy(
                            balance = result.data.balance.toETH().toString(),
                            currencyName = ETH_NAME
                        ) ?: UiState(
                            balance = result.data.balance.toETH().toString(),
                            currencyName = ETH_NAME
                        )
                    }
                }

                is Web3Result.Fail -> {
                    // TODO:
                }
            }

        }
    }

    private fun BigInteger.toETH(): BigDecimal? {
        return Convert.fromWei(this.toBigDecimal(), Convert.Unit.ETHER)
    }

    companion object {
        const val ETH_NAME = "SepoliaETH"
    }
}

inline fun <T> T?.createOrUpdate(default: T, update: T.() -> T): T {
    return this?.update() ?: default.update()
}