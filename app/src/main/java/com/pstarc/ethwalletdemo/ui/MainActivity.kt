package com.pstarc.ethwalletdemo.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.enableEdgeToEdge
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentResultListener
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.pstarc.ethwalletdemo.LoginHelper
import com.pstarc.ethwalletdemo.R
import com.pstarc.ethwalletdemo.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainActivity : AppCompatActivity(), FragmentResultListener {

    private val binding: ActivityMainBinding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContentView(binding.root)
        ViewCompat.setOnApplyWindowInsetsListener(binding.main) { v, insets ->
            val systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars())
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom)
            insets
        }

        supportFragmentManager.setFragmentResultListener(LoginFragment.REQUEST_KEY, this, this)

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.CREATED) {
                if (LoginHelper.walletAddress.isNullOrEmpty().not()) {
                    HomeActivity.start(this@MainActivity)
                    finish()
                }
            }

        }

        lifecycleScope.launch {
            lifecycle.repeatOnLifecycle(Lifecycle.State.STARTED) {
                hideAllLoginButton()
                showTextTypingEffect(getString(R.string.welcome), 80L)
                showAllLoginButton()
            }
        }

        setupView()
    }

    private fun setupView() {
        with(binding) {
            btnTopLogin.setOnClickListener {
                startLoginFragment()
            }
            btnCenterLogin.setOnClickListener {
                startLoginFragment()
            }
        }
    }

    private suspend fun showTextTypingEffect(text: String, delayMillis: Long = 100L): Boolean {
        return withContext(Dispatchers.Main) {
            with(binding) {
                txvWelcome.text = ""
                var displayText = ""
                text.forEach { char ->
                    delay(delayMillis)
                    displayText += char
                    txvWelcome.text = displayText
                }
                return@withContext true
            }
        }
    }

    private fun showAllLoginButton() {
        with(binding) {
            btnCenterLogin.isVisible = true
            btnTopLogin.isVisible = true
        }
    }

    private fun hideAllLoginButton() {
        with(binding) {
            btnCenterLogin.isGone = true
            btnTopLogin.isGone = true
        }
    }

    override fun onFragmentResult(requestKey: String, result: Bundle) {
        if (requestKey == LoginFragment.REQUEST_KEY) {
            HomeActivity.start(this)
            finish()
        }
    }

    private fun startLoginFragment() {
        LoginFragment.newInstance().show(supportFragmentManager, null)
    }

    companion object {

        private const val TAG = "MainActivity"

        fun start(context: Context) {
            context.startActivity(Intent(context, MainActivity::class.java))
        }
    }
}