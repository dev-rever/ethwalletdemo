package com.pstarc.ethwalletdemo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import androidx.fragment.app.setFragmentResult
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pstarc.ethwalletdemo.R
import com.pstarc.ethwalletdemo.databinding.FragmentTransactionBinding

class TransactionBottomSheet : BottomSheetDialogFragment() {

    private var _binding: FragmentTransactionBinding? = null
    private val binding
        get() = _binding ?: FragmentTransactionBinding.inflate(layoutInflater)
            .also { _binding = it }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.background =
            ResourcesCompat.getDrawable(resources, R.drawable.bg_rounded_corners_top, null)
        setupView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupView() {
        with(binding) {
            btnSend.setOnClickListener {
                val tAddress = etxTarget.text.toString() ?: ""
                val quantity = etxQuantity.text.toString() ?: "0"
                setFragmentResult(
                    REQUEST_KEY, bundleOf(
                        RESULT_ADDRESS to tAddress,
                        RESULT_QUANTITY to quantity
                    )
                )
                dismiss()
            }
        }
    }

    companion object {

        const val REQUEST_KEY = "transaction_request_key"
        const val RESULT_ADDRESS = "transaction_address_key"
        const val RESULT_QUANTITY = "transaction_quantity_key"

        fun newInstance() = TransactionBottomSheet()
    }
}