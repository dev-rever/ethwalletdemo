package com.pstarc.ethwalletdemo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.lifecycle.lifecycleScope
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pstarc.ethwalletdemo.R
import com.pstarc.ethwalletdemo.databinding.FragmentProcessingBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProcessingBottomSheet : BottomSheetDialogFragment() {

    private var _binding: FragmentProcessingBinding? = null
    private val binding
        get() = _binding ?: FragmentProcessingBinding.inflate(layoutInflater).also { _binding = it }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog?.setCancelable(false)
        binding.root.background =
            ResourcesCompat.getDrawable(resources, R.drawable.bg_rounded_corners_top, null)
    }

    override fun onStart() {
        super.onStart()
        viewLifecycleOwner.lifecycleScope.launch {
            showProcessDot()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private suspend fun showProcessDot(delayMillis: Long = 400L) {
        withContext(Dispatchers.Main) {

            var dot = 1

            with(binding) {
                val baseText = getString(R.string.processing)

                while (true) {

                    delay(delayMillis)

                    val sb = StringBuilder(baseText)
                    repeat(dot) {
                        sb.append(".")
                    }
                    txvProcessing.text = sb.toString()

                    if (dot >= 5) dot = 1 else dot++
                }
            }
        }
    }

    companion object {

        const val REQUEST_KEY = "transaction_request_key"

        fun newInstance() = ProcessingBottomSheet()
    }
}