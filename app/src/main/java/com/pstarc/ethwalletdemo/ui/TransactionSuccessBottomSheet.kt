package com.pstarc.ethwalletdemo.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.core.os.bundleOf
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.pstarc.ethwalletdemo.R
import com.pstarc.ethwalletdemo.databinding.FragmentTransactionSuccessBinding

class TransactionSuccessBottomSheet : BottomSheetDialogFragment() {

    private var _binding: FragmentTransactionSuccessBinding? = null
    private val binding
        get() = _binding ?: FragmentTransactionSuccessBinding.inflate(layoutInflater)
            .also { _binding = it }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.background =
            ResourcesCompat.getDrawable(resources, R.drawable.bg_rounded_corners_top, null)
        setupView()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupView() {
        with(binding) {
            txvFromAddr.text = arguments?.getString(FROM_ADDR) ?: ""
            txvToAddr.text = arguments?.getString(TO_ADDR) ?: ""
            txvTxHash.text = arguments?.getString(TX_HASH) ?: ""
            txvBlock.text = arguments?.getString(BLOCK) ?: ""
            btnDone.setOnClickListener {
                dismiss()
            }
        }
    }

    companion object {

        private const val FROM_ADDR = "from_addr"
        private const val TO_ADDR = "to_addr"
        private const val TX_HASH = "tx_hash"
        private const val BLOCK = "block"

        fun newInstance(fromAddr: String, toAddr: String, txHash: String, block: String) =
            TransactionSuccessBottomSheet().apply {
                arguments = bundleOf(
                    FROM_ADDR to fromAddr,
                    TO_ADDR to toAddr,
                    TX_HASH to txHash,
                    BLOCK to block
                )
            }
    }
}