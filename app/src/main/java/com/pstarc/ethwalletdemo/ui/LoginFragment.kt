package com.pstarc.ethwalletdemo.ui

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.core.os.bundleOf
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.pstarc.ethwalletdemo.LoginHelper
import com.pstarc.ethwalletdemo.databinding.FragmentLoginBinding
import com.pstarc.ethwalletdemo.touchToClearFocus

class LoginFragment : DialogFragment() {

    private var _binding: FragmentLoginBinding? = null
    private val binding
        get() = _binding ?: FragmentLoginBinding.inflate(layoutInflater)
            .also { _binding = it }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = Dialog(requireContext())
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.root.touchToClearFocus(requireActivity())
        setupView()
    }

    override fun onStart() {
        super.onStart()
        val window = dialog?.window
        window?.let {
            it.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)
            it.setBackgroundDrawableResource(android.R.color.transparent)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupView() {
        with(binding) {
            btnLogin.setOnClickListener {
                LoginHelper.login(
                    requireContext(),
                    etxAddress.text.toString(),
                    etxPrivateKey.text.toString()
                )
                setFragmentResult(REQUEST_KEY, bundleOf(RESULT_KEY to true))
                dismiss()
            }
        }
    }

    companion object {
        const val REQUEST_KEY = "login_fragment_request_key"
        const val RESULT_KEY = "login_fragment_result_key"

        @JvmStatic
        fun newInstance() = LoginFragment()
    }
}