package com.pstarc.ethwalletdemo.repo

import org.web3j.protocol.core.methods.response.EthGetBalance
import org.web3j.protocol.core.methods.response.TransactionReceipt

interface IWeb3Repository {

    suspend fun fetchBalance() : Web3Result<EthGetBalance>

    suspend fun sendEth(quantity: String, toAddr: String) : Web3Result<TransactionReceipt>
}