package com.pstarc.ethwalletdemo.repo

sealed class Web3Result<out T> {
    data class Success<T>(val data: T) : Web3Result<T>()
    data class Fail(val message: String) : Web3Result<Nothing>()
}