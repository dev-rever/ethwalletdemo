package com.pstarc.ethwalletdemo.repo

import com.pstarc.ethwalletdemo.Web3Client
import org.web3j.protocol.core.methods.response.EthGetBalance
import org.web3j.protocol.core.methods.response.TransactionReceipt

object Web3Repository : IWeb3Repository {

    private val client get() = Web3Client

    override suspend fun fetchBalance(): Web3Result<EthGetBalance> {
        runCatching {
            client.fetchBalance()
        }.getOrElse {
            return Web3Result.Fail(it.message.toString())
        }.let {
            return Web3Result.Success(it)
        }
    }

    override suspend fun sendEth(quantity: String, toAddr: String): Web3Result<TransactionReceipt> {

        val result = client.sendEth(quantity, toAddr)

        return if (result.isStatusOK) {
            Web3Result.Success(result)
        } else {
            Web3Result.Fail(result.logsBloom)
        }
    }
}