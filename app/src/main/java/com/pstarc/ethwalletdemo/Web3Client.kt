package com.pstarc.ethwalletdemo

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.web3j.crypto.Credentials
import org.web3j.crypto.RawTransaction
import org.web3j.crypto.TransactionEncoder
import org.web3j.protocol.Web3j
import org.web3j.protocol.core.DefaultBlockParameterName
import org.web3j.protocol.core.methods.response.EthGetBalance
import org.web3j.protocol.core.methods.response.TransactionReceipt
import org.web3j.protocol.http.HttpService
import org.web3j.tx.Transfer
import org.web3j.utils.Convert
import org.web3j.utils.Numeric
import java.math.BigInteger

object Web3Client {

    private val TAG = "Web3Client"

    private const val INFURA_API_KEY = "e1a0bb0ac5b1480e935667f525c01441"
    private const val MAINNET_URL = "https://mainnet.infura.io/v3/$INFURA_API_KEY"
    private const val SEPOLIA_URL = "https://sepolia.infura.io/v3/$INFURA_API_KEY"

    private val PRIVATE_KEY
        get() = LoginHelper.privateKey
            ?: throw IllegalStateException("private key must be not null, please login first.")
    private val WALLET_ADDRESS
        get() = LoginHelper.walletAddress
            ?: throw IllegalStateException("wallet address must be not null, please login first.")

    private val web3j: Web3j by lazy {
        Web3j.build(HttpService(SEPOLIA_URL))
    }

    fun initialize() {
        web3j
    }

    suspend fun fetchBalance(): EthGetBalance {
        return withContext(Dispatchers.IO) {
            return@withContext web3j.ethGetBalance(WALLET_ADDRESS, DefaultBlockParameterName.LATEST)
                .send()
        }
    }

    suspend fun startRawTransaction(quantity: String, toAddress: String) {
        return withContext(Dispatchers.IO) {

            val credential = Credentials.create(PRIVATE_KEY)
            val rawTran = RawTransaction.createEtherTransaction(
                BigInteger("1"),
                recommendGasPrice(),
                BigInteger("21000"),
                toAddress,
                Convert.toWei(quantity, Convert.Unit.ETHER).toBigInteger()
            )

            val signed = TransactionEncoder.signMessage(rawTran, credential)
            val hexValue = Numeric.toHexString(signed)

            val result = web3j.ethSendRawTransaction(hexValue).send()

            if (result.hasError()) {
                Log.d(TAG, "eth transaction fail reason: ${result.error.message}")
            } else {
                Log.d(
                    TAG,
                    "eth transaction successfully, transaction hash: ${result.transactionHash}"
                )
            }
        }
    }

    suspend fun recommendGasPrice(): BigInteger {
        return withContext(Dispatchers.IO) {
            return@withContext web3j.ethGasPrice().send().gasPrice ?: BigInteger("0")
        }
    }

    suspend fun sendEth(quantity: String, toAddress: String): TransactionReceipt {
        return withContext(Dispatchers.IO) {
            val credential = Credentials.create(PRIVATE_KEY)
            val result =
                Transfer.sendFunds(
                    web3j, credential, toAddress, quantity.toBigDecimal(), Convert.Unit.ETHER
                ).send()

            if (result.isStatusOK) {
                Log.d(
                    TAG, "eth transaction successfully, transaction hash: ${result.transactionHash}"
                )
            } else {
                result.logs.forEach {
                    Log.d(TAG, "eth transaction fail reason: $it")
                }
            }

            return@withContext result
        }
    }

    suspend fun getClientVersion(): String? {
        return withContext(Dispatchers.IO) {
            return@withContext web3j.web3ClientVersion().send().web3ClientVersion
        }
    }

    fun shotDown() {
        web3j.shutdown()
    }

}